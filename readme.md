# Mini linux

This project shows how to use buildstream (bst) with the freedesktop-sdk compilers to make a tiny linux system.

## Dependencies

You will need to install bst, a easy way todo this is to use docker. This ensures you have the same versions of the plugins etc as freedesktop does which is important so that we can use the freedesktop-sdk cache.

``` bash
podman pull registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/bst2:32bb82c3aecdef9b89b3b9731890e7b7891bb27e-amd64
mkdir -p ~/.cache/fd-sdk-bst2
podman run -it -v ~/.cache/fd-sdk-bst2:/root/.cache/buildstream/ -v ${PWD}:/src/ --workdir /src --privileged  registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/bst2:32bb82c3aecdef9b89b3b9731890e7b7891bb27e-amd64 bash
```

## Build

To build the full linux system you can just run bst with the image element.

``` bash
bst build image.bst
```

After bst has finished, the element will now be build and available in the cache.

## Checkout the elements

Now we can check out the files we have just built.

``` bash
bst artifact checkout image.bst
bst artifact checkout kernel.bst
```

We now have a full filesystem and a kernel.

## Run

We can use qemu to run the tiny linux system

``` bash
qemu-system-aarch64 -machine virt -cpu cortex-a57 -machine type=virt -nographic -smp 2 -m 4096 -kernel kernel/boot/Image -drive file=image/disk.img,format=raw -append "root=/dev/vda2 rw"
```
